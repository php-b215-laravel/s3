@extends('layouts.app')

@section('content')
<div class="d-grid gap-1 d-md-flex justify-content-md-end">
    <a href="/posts" class="btn btn-primary" type="button">Go Back</a>
</div>
    <h1>
        {{ $post->title }}
    </h1>

    <p>
        {{ $post->body }}
    </p>
@endsection